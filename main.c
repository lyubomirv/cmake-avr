#include <stdint.h>

#include <avr/io.h>
#include <util/delay.h>

int main()
{
	DDRB |= 1 << PINB0;
	PORTB = 0;

	while(1)
	{
		PORTB ^= 1 << PINB0;
		_delay_ms(250);
	}

	return 0;
}
