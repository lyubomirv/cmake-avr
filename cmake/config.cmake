set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c99 -fshort-enums -Wall -Werror -pedantic -pedantic-errors")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fshort-enums -Wall -Werror -pedantic -pedantic-errors")

set(CMAKE_C_FLAGS_RELEASE "-Os")
set(CMAKE_CXX_FLAGS_RELEASE "-Os")
set(CMAKE_C_FLAGS_RELWITHDEBINFO "-Os -save-temps -g -gdwarf-3 -gstrict-dwarf")
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-Os -save-temps -g -gdwarf-3 -gstrict-dwarf")
set(CMAKE_C_FLAGS_DEBUG "-O0 -save-temps -g -gdwarf-3 -gstrict-dwarf")
set(CMAKE_CXX_FLAGS_DEBUG "-O0 -save-temps -g -gdwarf-3 -gstrict-dwarf")

if(NOT ((CMAKE_BUILD_TYPE MATCHES Release) OR
		  (CMAKE_BUILD_TYPE MATCHES RelWithDebInfo) OR
		  (CMAKE_BUILD_TYPE MATCHES Debug) OR
		  (CMAKE_BUILD_TYPE MATCHES MinSizeRel)))
	set(
		CMAKE_BUILD_TYPE Release
		CACHE STRING "Choose cmake build type: Debug Release RelWithDebInfo MinSizeRel"
		FORCE
	)
endif()

message(STATUS "avrdude: ${AVRDUDE_EXECUTABLE}")
message(STATUS "avrdude programmer: ${AVRDUDE_PROGRAMMER}")
message(STATUS "avrdude port: ${AVRDUDE_PORT}")
message(STATUS "avrdude bitclock: ${AVRDUDE_BITCLOCK}")
message(STATUS "µCU: ${AVR_MCU}")
