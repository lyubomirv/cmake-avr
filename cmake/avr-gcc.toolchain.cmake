##########################################################################
# The toolchain requires some variables to be set.
#
# AVR_MCU (default: atmega8)
#	 Target AVR microcontroller
# AVRDUDE_EXECUTABLE (default: avrdude)
#	 avrdude executable
# AVRDUDE_PORT (default: usb)
#	 Port used for avrdude, e.g. usb
# AVRDUDE_PROGRAMMER (default: usbasp)
#	 Programmer hardware, e.g. usbasp
##########################################################################

set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR avr)
SET(CMAKE_SYSTEM_VERSION 1)
set(CMAKE_CROSSCOMPILING 1)

find_path(TOOLCHAIN_ROOT
	NAMES
		avr-gcc
	PATHS
		/usr/bin
		/usr/local/bin
		/bin
		$ENV{AVR_ROOT}
)
if(NOT TOOLCHAIN_ROOT)
	message(FATAL_ERROR "Toolchain root could not be found!!!")
endif(NOT TOOLCHAIN_ROOT)

set(CMAKE_FIND_ROOT_PATH ${TOOLCHAIN_ROOT})
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

set(CMAKE_C_COMPILER   "${TOOLCHAIN_ROOT}/avr-gcc${OS_SUFFIX}"     CACHE PATH "gcc"     FORCE)
set(CMAKE_CXX_COMPILER "${TOOLCHAIN_ROOT}/avr-g++${OS_SUFFIX}"     CACHE PATH "g++"     FORCE)
set(CMAKE_AR           "${TOOLCHAIN_ROOT}/avr-ar${OS_SUFFIX}"      CACHE PATH "ar"      FORCE)
set(CMAKE_LINKER       "${TOOLCHAIN_ROOT}/avr-ld${OS_SUFFIX}"      CACHE PATH "linker"  FORCE)
set(CMAKE_NM           "${TOOLCHAIN_ROOT}/avr-nm${OS_SUFFIX}"      CACHE PATH "nm"      FORCE)
set(CMAKE_OBJCOPY      "${TOOLCHAIN_ROOT}/avr-objcopy${OS_SUFFIX}" CACHE PATH "objcopy" FORCE)
set(CMAKE_OBJDUMP      "${TOOLCHAIN_ROOT}/avr-objdump${OS_SUFFIX}" CACHE PATH "objdump" FORCE)
set(CMAKE_STRIP        "${TOOLCHAIN_ROOT}/avr-strip${OS_SUFFIX}"   CACHE PATH "strip"   FORCE)
set(CMAKE_RANLIB       "${TOOLCHAIN_ROOT}/avr-ranlib${OS_SUFFIX}"  CACHE PATH "ranlib"  FORCE)
set(AVR_SIZE           "${TOOLCHAIN_ROOT}/avr-size${OS_SUFFIX}"	   CACHE PATH "size"    FORCE)

set(AVR_LINKER_LIBS "-lc -lm -lgcc")

# Some necessary tools and variables for AVR builds, which may not be defined yet
# - AVRDUDE_EXECUTABLE
# - AVRDUDE_PORT
# - AVRDUDE_BITCLOCK
# - AVRDUDE_PROGRAMMER
# - AVR_MCU

# Default avrdude executable
if(NOT AVRDUDE_EXECUTABLE)
	set(AVRDUDE_EXECUTABLE avrdude CACHE STRING "Set avrdude executable: avrdude" )
	find_program(AVRDUDE_EXECUTABLE avrdude)
endif(NOT AVRDUDE_EXECUTABLE)

# Default avrdude port
if(NOT AVRDUDE_PORT)
	set(AVRDUDE_PORT usb CACHE STRING "Set default avrdude port: usb")
endif(NOT AVRDUDE_PORT)

# Default avrdude bitclock
if(NOT AVRDUDE_BITCLOCK)
	set(AVRDUDE_BITCLOCK 10 CACHE STRING "Set default avrdude bitclock: 10" )
endif(NOT AVRDUDE_BITCLOCK)

# Default avrdude programmer (hardware)
if(NOT AVRDUDE_PROGRAMMER)
	set(AVRDUDE_PROGRAMMER usbasp CACHE STRING "Set default programmer hardware: usbasp")
endif(NOT AVRDUDE_PROGRAMMER)

# Default microcontroller (chip)
if(NOT AVR_MCU)
	set(AVR_MCU atmega8 CACHE STRING "Set default microcontroller: atmega8 (see 'avr-gcc --target-help' for valid values)")
endif(NOT AVR_MCU)
