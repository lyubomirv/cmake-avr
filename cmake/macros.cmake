##########################################################################
# add_avr_executable
# - TARGET_NAME
function(add_avr_executable TARGET_NAME)
	if(NOT ARGN)
		message(FATAL_ERROR "No source files given for ${TARGET_NAME}.")
	endif()
	
	# Define file names whilch will be used
	set(ELF_FILE ${TARGET_NAME}-${AVR_MCU}.elf)
	set(HEX_FILE ${TARGET_NAME}-${AVR_MCU}.hex)
	set(LST_FILE ${TARGET_NAME}-${AVR_MCU}.lst)
	set(EEPROM_IMAGE ${TARGET_NAME}-${AVR_MCU}-eeprom.hex)

	# Add elf target
	add_executable(${ELF_FILE}
		${ARGN})

	# Set compile and link flags for the elf target
	set_target_properties(
		${ELF_FILE}
		PROPERTIES
			COMPILE_FLAGS "-mmcu=${AVR_MCU}"
			LINK_FLAGS	 "-mmcu=${AVR_MCU} ${AVR_LINKER_LIBS}")
	
	# Generate the lst file
	add_custom_command(
		OUTPUT ${LST_FILE}
		COMMAND
			${CMAKE_OBJDUMP} -h -S ${ELF_FILE} > ${LST_FILE}
		DEPENDS ${ELF_FILE})

	# Create the hex file
	add_custom_command(
		OUTPUT ${HEX_FILE}
		COMMAND
			${CMAKE_OBJCOPY} -j .text -j .data -O ihex ${ELF_FILE} ${HEX_FILE}
		DEPENDS ${ELF_FILE})
	
	# Print the size of the compiled code
	if(APPLE)
		set(AVR_SIZE_ARGS -B)
	else()
		set(AVR_SIZE_ARGS -C;--mcu=${AVR_MCU})
	endif()
	add_custom_command(
		OUTPUT "print-size-${ELF_FILE}"
		COMMAND
			${AVR_SIZE} ${AVR_SIZE_ARGS} ${ELF_FILE}
		DEPENDS ${ELF_FILE})
	
	# Build the intel hex file for the device
	add_custom_target(
		${TARGET_NAME}
		ALL
		DEPENDS ${HEX_FILE} ${LST_FILE} "print-size-${ELF_FILE}")

	set_target_properties(
		${TARGET_NAME}
		PROPERTIES
			OUTPUT_NAME ${ELF_FILE})
	
	# Upload with avrdude
	add_custom_target(
		upload
		${AVRDUDE_EXECUTABLE} -p ${AVR_MCU} -c ${AVRDUDE_PROGRAMMER} -B ${AVRDUDE_BITCLOCK}
			-U flash:w:${HEX_FILE}
			-P ${AVRDUDE_PORT}
		DEPENDS ${HEX_FILE}
		COMMENT "Uploading ${HEX_FILE} to ${AVR_MCU} using ${AVRDUDE_PROGRAMMER}.")

	# Upload eeprom only with avrdude
	add_custom_target(
		upload_eeprom
		${AVRDUDE_EXECUTABLE} -p ${AVR_MCU} -c ${AVRDUDE_PROGRAMMER} -B ${AVRDUDE_BITCLOCK}
			-U eeprom:w:${EEPROM_IMAGE}
			-P ${AVRDUDE_PORT}
		DEPENDS ${EEPROM_IMAGE}
		COMMENT "Uploading ${EEPROM_IMAGE} to ${AVR_MCU} using ${AVRDUDE_PROGRAMMER}.")

	# Get status
	add_custom_target(
		status
		${AVRDUDE_EXECUTABLE} -p ${AVR_MCU} -c ${AVRDUDE_PROGRAMMER} -P ${AVRDUDE_PORT} -B ${AVRDUDE_BITCLOCK} -n -v
		COMMENT "Get status from ${AVR_MCU}.")
endfunction(add_avr_executable)
