# cmake-avr

CMake toolchain for AVR

Tested on Linux only.

## How to use:
1. Copy everything (without the `README.md`) into an empty project directory.
2. Rename the file `CMakeLists.sample.txt` to `CMakeLists.txt`.
3. In `CMakeLists.txt`, set the name of the project and executable, and set proper values for the microcontroller type (CMake variable `AVR_MCU`) and speed (CMake variable `MCU_SPEED`).
4. In the project directory, execute:

		mdkir build
		cd build
		cmake ..
5. If needed, adjust the programmer type and port, etc. using `ccmake`. This can also be done directly in the file `avr-gcc.toolchain.cmake`.

		make
6. Then to upload to an actual AVR microcontroller, while still in the directory `build`:

		cmake --build . --target upload
